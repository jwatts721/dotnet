using System;
using System.Linq;
using System.Threading.Tasks;
using BlazorApp.Extensions;

namespace BlazorApp.Data
{
    public class WeatherForecastService
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private static string GetSummary(int temp) {
            if (temp.IsLessThanOrEqual(10)) return Summaries[0];
            if (temp.IsBetween(11, 39)) return Summaries[1];
            if (temp.IsBetween(40, 59)) return Summaries[2];
            if (temp.IsBetween(60, 69)) return Summaries[3];
            if (temp.IsBetween(70, 75)) return Summaries[4];
            if (temp.IsBetween(76, 80)) return Summaries[5];
            if (temp.IsBetween(81, 95)) return Summaries[6];
            if (temp.IsBetween(96, 99)) return Summaries[7];
            if (temp.IsGreaterThanOrEqual(100)) return Summaries[8];
            return Summaries[0];
        }

        private static WeatherForecast GetWeatherForecastInstance(DateTime startDate, int index)
        {
            var rand = new Random();
            var forecast = new WeatherForecast() {
                Date = startDate.AddDays(index),
                TemperatureC = rand.Next(-20, 55)
            };
            forecast.Summary = GetSummary(forecast.TemperatureF);

            return forecast;
        }

        public Task<WeatherForecast[]> GetForecastAsync(DateTime startDate)
        {
            var rng = new Random();
            /*
            return Task.FromResult(Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = startDate.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            }).ToArray());
            */
            return Task.FromResult(Enumerable.Range(1, 5).Select(index => GetWeatherForecastInstance(startDate, index)).ToArray());
        }
    }
}
