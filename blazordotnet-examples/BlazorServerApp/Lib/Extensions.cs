using System;

namespace BlazorApp.Extensions {
    public static class Extensions
    {
        public static bool IsLessThan(this int value, int max)
        {
            return value < max;
        }
        public static bool IsLessThanOrEqual(this int value, int max)
        {
            return value <= max;
        }
        public static bool IsGreaterThan(this int value, int min)
        {
            return value > min;
        }
        public static bool IsGreaterThanOrEqual(this int value, int min)
        {
            return value >= min;
        }
        public static bool IsBetween(this int value, int min, int max)
        {
            return value >= min && value <= max;
        }
    }
}