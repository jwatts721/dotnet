﻿using EFSqlServer.Models;
using Microsoft.EntityFrameworkCore;

namespace EFSqlServer
{
    public class BloggingContext : DbContext
    {
        public DbSet<Blog>? Blogs { get; set; }
        public DbSet<Post>? Posts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options) {
            if (!options.IsConfigured)
            {
                options.UseSqlServer("Server=127.0.0.1;Database=Property_Management;User Id=pmuser;Password=pmuser123;");
            }
        }
    }
}
