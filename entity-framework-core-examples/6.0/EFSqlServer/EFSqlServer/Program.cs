﻿using EFSqlServer;
using EFSqlServer.Models;

// See https://aka.ms/new-console-template for more information
Console.WriteLine("EF SQL Server Demo!");

var dbcontext = new BloggingContext();
//if (!dbcontext.Database.EnsureCreated())
//{
//    Console.WriteLine("Error connecting to the database...");
//    return;
//}

var blog = new Blog();
blog.Url = "https://exampleblog.com";
dbcontext.Add(blog);
dbcontext.SaveChanges();

blog = new Blog();
blog.Url = "https://newblog.com";
dbcontext.Add(blog);
dbcontext.SaveChanges();

var blogs = dbcontext.Blogs;
if (blogs != null)
{
    Console.WriteLine($"Total number of blogs: {blogs.Count()}");
}



